import streamlit as st
import pandas as pd

df = pd.read_csv('src/sample.csv')


st.title('Showdata')
st.dataframe(
    df,
    column_config={
        "img": st.column_config.ImageColumn(
            "Preview Image", help="Streamlit app preview screenshots"
        ),
    },
    hide_index=True,
)
