# Study Table and Img into Streamlit
This repo contains a simple example of streamlit that show a pandas dataframe and render images inside.

|  base | double click on image cell  |
|---|---|
|  ![simple table show](img/simple.png) | ![zoommed on image](img/zoomed.png)  |

## How to run

```
docker compose up
# or docker-compose up

# go to localhost:8000
```

# References
* python
* docker, docker compose
* streamlit
* pandas